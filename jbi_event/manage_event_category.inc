<?php

/**
 * Form with 'add more' and 'remove' buttons.
 *
 * This form has a button to "add more" - add another textfield, and
 * the corresponding "remove" button.
 *
 * It works equivalently with javascript or not, and does the same basic steps
 * either way.
 *
 * The basic idea is that we build the form based on the setting of
 * $form_state['num_names']. The custom submit functions for the "add-one"
 * and "remove-one" buttons increment and decrement $form_state['num_names']
 * and then force a rebuild of the form.
 *
 * The $no_js_use argument : When set, it prevents
 * '#ajax' from being set, thus making it as if javascript
 * were disabled in the browser.
 */
function add_event_cat_form($form, $form_state, $no_js_use = FALSE) {
    $form = array();
    $event_arr = array();
    $form['#tree'] = TRUE;
    if (!empty($event_arr)) {
        $events = theme('event_list_form');
    }
    $form['event_cat_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Event Category'),
      // Set up the wrapper so that AJAX will be able to replace the fieldset.
      '#prefix' => '<div id="event-cat-fieldset-wrapper">',
      '#suffix' => '</div>',
    );
    // Build the fieldset with the proper number of names. We will use
    // $form_state['num_names'] to determine the number of textfields to build.
    if (empty($form_state['num_names'])) {
        $form_state['num_names'] = 1;
    }
    for ($i = 0; $i < $form_state['num_names']; $i++) {
        $form['event_cat_fieldset']['event-cats'][$i]['cat'] = array(
          '#type' => 'fieldset',
          '#title' => t('Event'),
        );
        $form['event_cat_fieldset']['event-cats'][$i]['cat']['event_cat_name'] = array(
          '#type' => 'textfield',
          '#title' => t('Event Category Name'),
          '#required' => TRUE,
        );
        $form['event_cat_fieldset']['event-cats'][$i]['cat']['event_cat_description'] = array(
          '#type' => 'textarea',
          '#title' => t('Event Category Description'),

        );
              
       
    }
    $form['event_cat_fieldset']['add_event_cat'] = array(
      '#type' => 'submit',
      '#value' => t('Add one more Event Category'),
      '#submit' => array('event_cat_add_more_add_one'),
      '#limit_validation_errors' => array(), 
      // See the examples in ajax_example.module for more details on the
      // properties of #ajax.
      '#ajax' => array(
        'callback' => 'event_cat_add_more_callback',
        'wrapper' => 'event-cat-fieldset-wrapper',
      ),
    );
    if ($form_state['num_names'] > 1) {
        $form['event_cat_fieldset']['remove_event_cat'] = array(
          '#type' => 'submit',
          '#value' => t('Remove Event Category'),
          '#submit' => array('event_cat_add_more_remove_one'),
          '#limit_validation_errors' => array(), 
          '#ajax' => array(
            'callback' => 'event_cat_add_more_callback',
            'wrapper' => 'event-cat-fieldset-wrapper',
          ),
        );
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    // This simply allows us to demonstrate no-javascript use without
    // actually turning off javascript in the browser. Removing the #ajax
    // element turns off AJAX behaviors on that element and as a result
    // ajax.js doesn't get loaded.
    // For demonstration only! You don't need this.
    if ($no_js_use) {
        // Remove the #ajax from the above, so ajax.js won't be loaded.
        if (!empty($form['event_cat_fieldset']['remove_event_cat']['#ajax'])) {
            unset($form['event_cat_fieldset']['remove_event_cat']['#ajax']);
        }
        unset($form['event_cat_fieldset']['add_event_cat']['#ajax']);
    }
    return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function event_cat_add_more_callback($form, $form_state) {
    return $form['event_cat_fieldset'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function event_cat_add_more_add_one($form, &$form_state) {
    $form_state['num_names'] ++;
    $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function event_cat_add_more_remove_one($form, &$form_state) {
    if ($form_state['num_names'] > 1) {
        $form_state['num_names'] --;
    }
    $form_state['rebuild'] = TRUE;
}

/**
 * Final submit handler.
 *
 * Reports what values were finally set.
 */
function add_event_cat_form_submit($form, &$form_state) {
    $eve_cats = array();
    try {
        foreach ($form_state['values']['event_cat_fieldset']['event-cats'] as $key => $value) {
            $aid = db_insert('jbi_event_category')
                ->fields(array(
                  'event_cat_name' => $value['cat']['event_cat_name'],
                  'event_cat_description' => $value['cat']['event_cat_description'],
                ))
                ->execute();
            $eve_cats[] = $value['cat']['event_cat_name'];
        }
    }
    catch (Exception $e) {
        drupal_set_message(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
    }

    $output = t('The Event Category added are : @names', array(
      '@names' => implode(', ', $eve_cats),
        )
    );
    drupal_set_message($output);
    drupal_goto("admin/config/jbi-event-config/list-event-category");
}

/**
 * Event Category Edit form.
 *
 */
function edit_event_cat_form($form, $form_state, $arg) {

    $results = db_query("SELECT * FROM {jbi_event_category} where cid = :cid order by event_cat_name DESC ", array(':cid' => $arg))->fetchObject();
    $form['event_cat_fieldset']['event-cats']['cat'] = array(
      '#type' => 'fieldset',
      '#title' => t('Event Category'),
    );
    $form['event_cat_fieldset']['event-cats']['cat']['cid'] = array(
      '#type' => 'hidden',
      '#value' => $results->cid
    );
    $form['event_cat_fieldset']['event-cats']['cat']['event_cat_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Event Category Name'),
      '#required' => TRUE,
      '#default_value' => $results->event_cat_name
    );
    $form['event_cat_fieldset']['event-cats']['cat']['event_cat_description'] = array(
      '#type' => 'textarea',
      '#title' => t('Event Category Description'),
      '#required' => TRUE,
      '#default_value' => $results->event_cat_description
    );
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
    return $form;
}

/**
 * Event Category Edit form Submit handler.
 *
 */
function edit_event_cat_form_submit($form, $form_state) {
    $values = $form_state['values'];
    db_query(
        "UPDATE {jbi_event_category} SET event_cat_name = :event_cat_name, event_cat_description = :event_cat_description WHERE cid = :cid", array(':event_cat_name' => $values['event_cat_name'], ':event_cat_description' => $values['event_cat_description'], ':cid' => $values['cid'])
    );
    drupal_set_message(t('Event Category has been Updated successfully.'));
    drupal_goto("admin/config/jbi-event-config/list-event-category");
}

/**
 * List all the Event Category.
 *
 */
function list_event_category() {
  $query = "SELECT * FROM {jbi_event_category} order by cid ASC,event_cat_name ASC";
  $result_count = db_query($query)->rowCount();
  if($result_count < 1) {
    drupal_set_message("Please add one or more Event Category.", 'warning');
    drupal_goto("admin/config/jbi-event-config/jbi-event-category-add");
  }
  $results = db_query($query);
  $header = array('Name' =>  'Name','Description' => 'Description', 'action' => 'Action');
  foreach ($results as $record) {
    $edit_link = l('Edit' , 'admin/config/jbi-event-config/event-cat/'.$record->cid.'/edit');
    $delete_link = l('Delete' , 'admin/config/jbi-event-config/remove-event-category/delete/'.$record->cid.'/'. $record->event_cat_name);
    $data = array("Name" => $record->event_cat_name, "Description" => $record->event_cat_description, 'action' =>
       $edit_link . " | " . $delete_link );
    $rows[] = $data;
   } 
  $output = theme('table', 
           array('header' => $header, 
                  'rows' => $rows ));
  return $output;
}

/**
 * Remove Event Category.
 *
 */
function remove_event_cat_confirm($form, &$form_state, $cid, $event_cat) {
    $form['_events_cat'] = array(
      '#type' => 'value',
      '#value' => $cid,);
    $form['_events_cat_name'] = array(
      '#type' => 'value',
      '#value' => $event_cat,);

    return confirm_form($form, t('Are you sure you want to delete ' . $event_cat . ' Event Category?'), isset($_GET['destination']) ? $_GET['destination'] : "admin/config/jbi-event-config/list-event-category", t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Remove Event Category confirm.
 *
 */
function remove_event_cat_confirm_submit($form, &$form_state) {
    $form_values = $form_state['values'];
    if ($form_state['values']['confirm']) {
        $event_cat = $form_state['values']['_events_cat'];
        $event_cat_name = $form_state['values']['_events_cat_name'];
        $result = db_query("DELETE FROM {jbi_event_category} where cid='{$event_cat}'");
        drupal_set_message(t(  $event_cat_name . ' has been deleted successfully.'));
    }
    drupal_goto("admin/config/jbi-event-config/list-event-category");
}
