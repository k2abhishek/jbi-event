  The JBI Event Module is built to test the ability of me (Kumar Abhishek)

  In this Module, I tried to implement a Event Management system in a very small level.

  Following are the points which are covered :

	  a. The ability for Admin to create Event and Event Categories.
	  b. Front end – List all events, grouped by category.
	  c. The ability to subscribe to the event. All event subscribers should be stored in DB and listed to the Admin. 

 

  There are many things which are not taken into consideration like :

    1) Subsciption : A Subscription must have timestamp to know when the user has applied for the subscription for an event.
    				 A subscription form for general user must have a anit-spaming field i.e. captcha but i have not used because 
    				 it will require dependency on other module like "Captcha"
    2) Event and Event Category Description Length : This is not taken into the consideration while using it in the form vis a vis
                                                     Displaying in the tabular format. We can use substr to cut its length while displaying it. And Also, We might have applied validation while using it in the form.
    3) User Friendly UI : I have also not used some user friendly UI like pop-up calendar just to make sure it works in a fresh drupal installation and without any help of other module
    4) We could have also used taxonomy and views for the Event Category and listing the values respectively. 

  Database Assumption and structure :

    I have taken following tables :

    a) jbi_event - To store the Event details 
         -> eid, event_name, event_description, event_start_date, event_end_date, event_location, event_category (cid is taken from jbi_event_category )

    b) jbi_event_category - To store Event Category details
         -> cid, event_cat_name, event_cat_description
    c) jbi_subscriber - To store Subscriber details
         -> sid, event_sub_name, event_sub_email
    d) jbi_event_subscriber -> To store the many to many relationship of Subscriber and Event
         -> seid, sid, eid

    To get more information regarding database structure, please look at jbi_event.install file


Admin User Interface :

Permission : I have create a permission to configure our requirement, we have one permission named "Administer JBI Event" and you can find at the Permission admin UI i.e. <base url>/admin/people/permissions and set the permission for desired roles.

As per the requirement, The User can do following thing from Admin UI after geeting the proper permission or is an admin User i.e. user id 1 :

1) Add Event Category ( Add JBI Event Category ): To Add Event Category, Please got to <base url>/admin/config/jbi-event-config/jbi-event-category-add
       -> This is having a facility to add multiple Event category at a time, so that we can avoid multiple redirects and page load. And make it more user friendly

2) Add Event ( Add JBI Event/ Configure JBI Event ): To add event, Please go to : <base url>/admin/config/jbi-event-config. But note if you have not added Event category then it will redirect you to "Add Event Category" i.e <base url>/admin/config/jbi-event-config/jbi-event-category-add
       -> This is having a facility to add multiple Events at a time, so that we can avoid multiple redirects and page load. And make it more user friendly

3) List Event Categories : To check the list of Event Category, please got to <base url>/admin/config/jbi-event-config/list-event-category  
    ->   From the list, we can also use different action like Edit and Delete categories like 
    		# To Edit category : we can use --> <base url>/admin/config/jbi-event-config/event-cat/<category id>/edit
    		# To Delete category : we can use --> <base url>/admin/config/jbi-event-config/remove-event-category/delete/<category id>/<category name>

4) List Events : To check the list of Events, please got to <base url>/admin/config/jbi-event-config/list-events  
    ->   From the list, we can also use different action like Edit , Delete and list of subscriber of the Event like 
    		# To Edit : we can use --> <base url>/admin/config/jbi-event-config/event/<event id>/edit
    		# To Delete : we can use --> <base url>/admin/config/jbi-event-config/remove-event/delete/<event id>/<event name>
    		# To list Subscriber : we can use --> <base url>/admin/config/jbi-event-config/list-events/subcriber/<event id>

General UI:

1) As per requirement, A genral user can see a list of event by category, so i have applied accordion to show the Events grouped by category.
      -> The URl for <base url>/list-event-by-category
      -> Here i have given a link whick looks like a button named "Subscribe" for the Event Subscription i.e <base url>subscribe-event/<event id>
2) A Subscripttion form :
      -> A user can fill the form and apply for Subscription

Once the subscription is applied by the user, it can be displayed on the Admin user event subcription list.


@Known Bugs: Since, It is just made to demonstrate my ability to work, i have not tested it extensively and ther can be bug in the module like :

   a) At Event and Event Category adding page, at first when you click at "Add More" button , it does not give us the another form but after successive click it works.

      



