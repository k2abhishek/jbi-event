<?php

/**
 * Form with 'add more' and 'remove' buttons.
 *
 * This form has a button to "add more" - add another textfield, and
 * the corresponding "remove" button.
 *
 * It works equivalently with javascript or not, and does the same basic steps
 * either way.
 *
 * The basic idea is that we build the form based on the setting of
 * $form_state['num_names']. The custom submit functions for the "add-one"
 * and "remove-one" buttons increment and decrement $form_state['num_names']
 * and then force a rebuild of the form.
 *
 * The $no_js_use argument : When set, it prevents
 * '#ajax' from being set, thus making it as if javascript
 * were disabled in the browser.
 */
function add_event_form($form, $form_state, $no_js_use = FALSE) {
    $form = array();
    $event_cat_arr = array();
    $event_cat_result = db_query("select cid, event_cat_name from {jbi_event_category}");
    foreach ($event_cat_result as $record) {
        $event_cat_arr[$record->cid] = $record->event_cat_name;
    }
    if(empty($event_cat_arr)) {
      drupal_set_message("Please add one or more Event Category before adding event", 'warning');
      drupal_goto("admin/config/jbi-event-config/jbi-event-category-add");
    }
    $form['#tree'] = TRUE;
    $form['event_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Event'),
      // Set up the wrapper so that AJAX will be able to replace the fieldset.
      '#prefix' => '<div id="event-fieldset-wrapper">',
      '#suffix' => '</div>',
    );
    // Build the fieldset with the proper number of names. We will use
    // $form_state['num_names'] to determine the number of textfields to build.
    if (empty($form_state['num_names'])) {
        $form_state['num_names'] = 1;
    }
    for ($i = 0; $i < $form_state['num_names']; $i++) {
        $form['event_fieldset']['events'][$i]['event'] = array(
          '#type' => 'fieldset',
          '#title' => t('Event'),
        );
        $form['event_fieldset']['events'][$i]['event']['event_name'] = array(
          '#type' => 'textfield',
          '#title' => t('Event Name'),
          '#required' => TRUE,
        );
        $form['event_fieldset']['events'][$i]['event']['event_description'] = array(
          '#type' => 'textarea',
          '#required' => TRUE,
          '#title' => t('Event Description'),
        );
        $form['event_fieldset']['events'][$i]['event']['event_start_date'] = array(
          '#type' => 'date',
          '#required' => TRUE,
          '#title' => t('Start Date'), 
        );
        $form['event_fieldset']['events'][$i]['event']['event_end_date'] = array(
          '#type' => 'date',
          '#required' => TRUE,
          '#title' => t('End Date'),
        );
        $form['event_fieldset']['events'][$i]['event']['event_location'] = array(
          '#type' => 'textfield',
          '#required' => TRUE,
          '#title' => t('Event Location'),
        );
        $form['event_fieldset']['events'][$i]['event']['event_category'] = array(
         '#type' => 'select',
         '#title' => t('Event Category'),
         '#required' => TRUE,
         '#options' => $event_cat_arr,
       
        );
    }
    $form['event_fieldset']['add_event'] = array(
      '#type' => 'submit',
      '#value' => t('Add one more Event'),
      '#limit_validation_errors' => array(), 
      '#submit' => array('event_add_more_add_one'),
      // See the examples in ajax_example.module for more details on the
      // properties of #ajax.
      '#ajax' => array(
        'callback' => 'event_add_more_callback',
        'wrapper' => 'event-fieldset-wrapper',
      ),
    );
    if ($form_state['num_names'] > 1) {
        $form['event_fieldset']['remove_events'] = array(
          '#type' => 'submit',
          '#value' => t('Remove Events'),
          '#submit' => array('event_add_more_remove_one'),
          '#limit_validation_errors' => array(), 
          '#ajax' => array(
            'callback' => 'event_add_more_callback',
            'wrapper' => 'event-fieldset-wrapper',
          ),
        );
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    // This simply allows us to demonstrate no-javascript use without
    // actually turning off javascript in the browser. Removing the #ajax
    // element turns off AJAX behaviors on that element and as a result
    // ajax.js doesn't get loaded.
    // For demonstration only! You don't need this.
    if ($no_js_use) {
        // Remove the #ajax from the above, so ajax.js won't be loaded.
        if (!empty($form['event_fieldset']['remove_events']['#ajax'])) {
            unset($form['event_fieldset']['remove_events']['#ajax']);
        }
        unset($form['event_fieldset']['add_event']['#ajax']);
    }
    return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function event_add_more_callback($form, $form_state) {
  return $form['event_fieldset'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function event_add_more_add_one($form, &$form_state) {
  $form_state['num_names'] ++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function event_add_more_remove_one($form, &$form_state) {
  if ($form_state['num_names'] > 1) {
      $form_state['num_names'] --;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Form Validation
 *
 * Reports what values were not properly set like End date must be greater than Start date.
 */
function add_event_form_validate($form = NULL, &$form_state = NULL, $form_id = NULL) {
  date_default_timezone_set('Asia/Calcutta'); 
  foreach ($form_state['values']['event_fieldset']['events'] as $key => $value) {
    $start_date = strtotime($value['event']['event_start_date']['month']."/".$value['event']['event_start_date']['day']."/".$value['event']['event_start_date']['year'] );
    $end_date = strtotime($value['event']['event_end_date']['month']."/".$value['event']['event_end_date']['day']."/".$value['event']['event_end_date']['year']);
    $datediff =  $end_date - $start_date ;
    $is_greater =  floor($datediff / (60 * 60 * 24)) . "<br/>";
    if($is_greater < 0 ) {
      form_set_error('event_fieldset[events]['.$key.'][event][event_start_date]', t('Please enter End Date greater than start Date.'));
    }
  }

}

/**
 * Final submit handler.
 *
 * Reports what values were finally set.
 */
function add_event_form_submit($form, &$form_state) {
    $eves = array();
    try {

      foreach ($form_state['values']['event_fieldset']['events'] as $key => $value) {
        $start_date = strtotime($value['event']['event_start_date']['month']."/".$value['event']['event_start_date']['day']."/".$value['event']['event_start_date']['year'] );
          $end_date = strtotime($value['event']['event_end_date']['month']."/".$value['event']['event_end_date']['day']."/".$value['event']['event_end_date']['year']);
          $eid = db_insert('jbi_event')
              ->fields(array(
                'event_name' => $value['event']['event_name'],
                'event_description' => $value['event']['event_description'],
                'event_start_date' => $start_date ,
                'event_end_date' => $end_date ,
                'event_location' => $value['event']['event_location'],
                'event_category' => $value['event']['event_category'],
              ))
              ->execute();
          $eves[] = $value['event']['event_name'] . " - ".  date("jS F, Y", $start_date) . " , End Date  - " . date("jS F, Y", $end_date)  ;
      }
    }
    catch (Exception $e) {
        drupal_set_message(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
    }

    $output = t('These Events added are : @names', array(
      '@names' => implode(', ', $eves),
        )
    );
    drupal_set_message($output);
}

/*
* Edit Event Form
*/
function edit_event_form($form, $form_state, $arg) {

    $results = db_query("SELECT * FROM {jbi_event} where eid = :eid order by event_name DESC ", array(':eid' => $arg))->fetchObject();
   
    $start_date_arr =  getdate($results->event_start_date);
    $start_date =  array('year' => $start_date_arr['year'], 'month' => $start_date_arr['mon'], 'day' => $start_date_arr['mday']);
    $end_date_arr =  getdate($results->event_end_date);
    $end_date = array('year' => $end_date_arr['year'], 'month' => $end_date_arr['mon'], 'day' => $end_date_arr['mday']);
    $event_cat_arr = array();
    $event_cat_result = db_query("select cid, event_cat_name from {jbi_event_category}");
    foreach ($event_cat_result as $record) {
        $event_cat_arr[$record->cid] = $record->event_cat_name;
 
    }
    
    if(empty($event_cat_arr)) {
      drupal_set_message("Please add one or more Event Category before adding event", 'warning');
      drupal_goto("admin/config/jbi-event-config/jbi-event-category-add");
    }
    $form['event_fieldset']['events']['event'] = array(
      '#type' => 'fieldset',
      '#title' => t('Event'),
    );
    $form['event_fieldset']['events']['event']['eid'] = array(
      '#type' => 'hidden',
      '#value' => $results->eid
    );
    $form['event_fieldset']['events']['event']['event_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Event Name'),
      '#required' => TRUE,
      '#default_value' => $results->event_name
    );
    $form['event_fieldset']['events']['event']['event_description'] = array(
      '#type' => 'textarea',
      '#title' => t('Event Description'),
      '#required' => TRUE,
      '#default_value' => $results->event_description
    );
    $form['event_fieldset']['events']['event']['event_start_date'] = array(
      '#type' => 'date',
      '#title' => t('Start Date'),
      '#default_value' => $start_date,
      '#required' => TRUE,

    );
    $form['event_fieldset']['events']['event']['event_end_date'] = array(
      '#type' => 'date',
      '#title' => t('End Date'),
      '#default_value' => $end_date,    
      '#required' => TRUE,
    );
    $form['event_fieldset']['events']['event']['event_location'] = array(
      '#type' => 'textfield',
      '#title' => t('Event Location'),
      '#required' => TRUE,
      '#default_value' => $results->event_location
    );
    $form['event_fieldset']['events']['event']['event_category'] = array(
      '#type' => 'select',
      '#title' => t('Event Category'),
      '#required' => TRUE,
      '#options' =>  $event_cat_arr,
      '#default_value' => $results->event_category
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
    return $form;
}

/**
 * Form Validation
 *
 * Reports what values were not properly set like End date must be greater than Start date.
 */

function edit_event_form_validate($form = NULL, &$form_state = NULL, $form_id = NULL) {
  date_default_timezone_set('Asia/Calcutta'); 
  $values = $form_state['values'];
    $start_date = strtotime($values['event_start_date']['month']."/".$values['event_start_date']['day']."/".$values['event_start_date']['year'] );
    $end_date = strtotime($values['event_end_date']['month']."/".$values['event_end_date']['day']."/".$values['event_end_date']['year']);
    $datediff =  $end_date - $start_date ;
    $is_greater =  floor($datediff / (60 * 60 * 24)) . "<br/>";
    if($is_greater < 0 ) {
      form_set_error('event_end_date[month]', t('Please enter End Date greater than start Date.'));
  }
  
 }

 /**
 * Final submit handler.
 *
 * Reports what values were finally set.
 */
function edit_event_form_submit($form, $form_state) {
  $values = $form_state['values'];
  $start_date = strtotime($values['event_start_date']['month']."/".$values['event_start_date']['day']."/".$values['event_start_date']['year'] );
  $end_date = strtotime($values['event_end_date']['month']."/".$values['event_end_date']['day']."/".$values['event_end_date']['year']);
  db_query(
      "UPDATE {jbi_event} SET event_name = :event_name, event_description = :event_description, event_start_date = :event_start_date, event_end_date = :event_end_date, event_location = :event_location, event_category = :event_category WHERE eid = :eid", array(':event_name' => $values['event_name'], ':event_description' => $values['event_description'], ':event_start_date' =>  $start_date , ':event_end_date' => $end_date, ':event_location' => $values['event_location'], ':event_category' => $values['event_category'], ':eid' => $values['eid'])
  );
  drupal_set_message(t('Event has been Updated successfully.'));
  drupal_goto("admin/config/jbi-event-config/list-events");
}

/**
 * List Event Callback.
 *
 * List All the Events and their details with different action to perform.
 */
function list_event() {
  $query = "SELECT * FROM {jbi_event} order by eid ASC,event_name ASC ";
  $result_count = db_query($query)->rowCount();
  if($result_count < 1) {
    drupal_set_message("Please add one or more Event", 'warning');
    drupal_goto("admin/config/jbi-event-config/jbi-event-category-add");
  }
  $results = db_query($query);
  $header = array('Name' =>  'Name','Description' => 'Description', 'Location' => 'Location', 'Date' => 'Event Date',  'action' => 'Action');
  foreach ($results as $record) {
    $date = date("d M Y" , $record->event_start_date ) . " to " . date("d M Y" , $record->event_end_date );
    $edit_link = l('Edit' , 'admin/config/jbi-event-config/event/'.$record->eid.'/edit');
    $delete_link = l('Delete' , 'admin/config/jbi-event-config/remove-event/delete/'.$record->eid.'/'. $record->event_name);
    $subscriber_list = l('List Subscriber' , 'admin/config/jbi-event-config/list-events/subcriber/'.$record->eid);
    $data = array("Name" => $record->event_name, "Description" => $record->event_description, "Location" => $record->event_location, 'Date' =>  $date ,'action' =>
       $edit_link . " | " . $delete_link . " | ". $subscriber_list);
    $rows[] = $data;
   } 
  $output = theme('table', 
           array('header' => $header, 
                  'rows' => $rows ));
  return $output;
}


/**
 * Remove Event.
 */
function remove_event_confirm($form, &$form_state, $eid, $event) {
  $form['_events'] = array(
    '#type' => 'value',
    '#value' => $eid,);
  $form['_events_name'] = array(
    '#type' => 'value',
    '#value' => $event,);

  return confirm_form($form, t('Are you sure you want to delete ' . $event . ' Event?'), isset($_GET['destination']) ? $_GET['destination'] : "admin/config/jbi-event-config/list-events", t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Remove Event confirmation Page.
 */
function remove_event_confirm_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  if ($form_state['values']['confirm']) {
      $event = $form_state['values']['_events'];
      $result = db_query("DELETE FROM {jbi_event} where eid='{$event}'");
      drupal_set_message(t('Event ' . $form_state['values']['_events_name'] . ' has been deleted successfully.'));
  }
  drupal_goto("admin/config/jbi-event-config/list-events");
}

/**
 * List the Subscriber of a particular Event.
 */
function list_subscriber($eid) {

  $query = "select s.event_sub_name, s.event_sub_email, e.event_name  from jbi_subscriber s INNER JOIN jbi_event_subscriber es on s.sid = es.sid INNER JOIN jbi_event e on e.eid = es.eid where e.eid = :eid";
  $result_count = db_query($query, array(':eid' => $eid))->rowCount();
  if($result_count < 1) {
   $output = "<h3>Noone has yet subscribed for this event.</h3>";
   return $output;
  }
  $result = db_query($query, array(':eid' => $eid)) ;
  $header = array('Subscriber Name' =>  'Name','Subscriber Email' => 'Email');
  foreach ($result as $record) {
    drupal_set_title($record->event_name."'s Subscriber list");
    $title = $record->event_name;
    $data = array("Subscriber Name" => $record->event_sub_name, "Subscriber Email" => $record->event_sub_email);
    $rows[] = $data;
   } 
  $output = "<h1>".$title ."'s Subscriber list</h1>";
  $output .= theme('table', 
           array('header' => $header, 
                  'rows' => $rows ));
  return $output;
}
