<!-- Implement Accordion for the events by their category -->
<div id="accordion">
<?php 
 foreach($events as $key => $val) { 
 	$event_cat_arr = explode("-", $key);
    $event_cat_name = trim($event_cat_arr[1]);
 ?>
 	 <h3> <?php print $event_cat_name ; ?> </h3> 
	 <div>
	 <ul>
	 <?php foreach ($val as  $event) { 
          $date = date("d M Y" , $event->event_start_date ) . " to " . date("d M Y" , $event->event_end_date );
	 	?>
	 		<li>
	 		 <strong><?php print $event->event_name;?></strong>	
	 		 <p><?php print $event->event_description;?></p>
	 		 <div class="event-location"><?php print $event->event_location;?></div>
	 		 <div class="event-date"><?php print $date ;?></div>
	 		 <div class="eventsubscribe"><?php print l('Subscribe', 'subscribe-event/'.$event->eid, array('attributes' => array('class' => array('button','event-subscribe-button'))));?></div>
	 		</li>
	  <?php } ?>
	  </ul> 
	 </div>
 <?php } ?>
</div>
